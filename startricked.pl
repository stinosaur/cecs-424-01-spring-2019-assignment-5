% "Star Tricked"
% Four ufo enthusiasts made sightings of different ufos their neighborhood.
% Each person reported his or her sighting on a different day (tues-fri)

person(barrada).
person(gort).
person(klatu).
person(nikto).

ufo(balloon).
ufo(clothesline).
ufo(frisbee).
ufo(watertower).

solve :-
    person(MsBarrada), person(MsGort), person(MrKlatu), person(MrNikto),
    all_different([MsBarrada, MsGort, MrKlatu, MrNikto]),

    ufo(BarradaUfo), ufo(GortUfo), ufo(KlatuUfo), ufo(NiktoUfo),
    all_different([BarradaUfo, GortUfo, KlatuUfo, NiktoUfo]),

    Triples = [ [MsBarrada, BarradaUfo, tuesday],
                [MsGort, GortUfo, wednesday],
                [MrKlatu, KlatuUfo, thursday],
                [MrNikto, NiktoUfo, friday] ],

   % 1. Mr. Klatu made sighting EARLIER in the week than person who saw BALLOON,
   %   but at some point LATER in the week than person who saw FRISBEE (who isn't Ms. Gort)
    \+ member([klatu, balloon, _], Triples),  % Klatu did not see a balloon
    \+ member([klatu, frisbee, _], Triples),  % Klatu did not see a frisbee
    \+ member([gort, frisbee, _], Triples),   % Gort did not see a frisbee

    earlier([klatu, _, _], [_, balloon, _], Triples),     % Klatu made his sighting earlier than when the balloon sighting was made
    earlier([_, frisbee, _], [klatu, _, _], Triples),     % The frisbee sighting was made was earlier than when klatu made his sighting

    % 2. Friday's sighting was made by either Ms. Barrada or the one who saw a clothesline (or both)
    (member([barrada, _, friday], Triples) ;              % Either Barrada made Friday's sighting OR
     member([_, clothesline, friday], Triples)),          % The person who saw the clothesline made Friday's sighting

    % 3. Mr. Nikto did not make his sighting on Tuesday
    \+ member([nikto, _, tuesday], Triples),              % Nikto did not make his sighting on Tuesday

    % 4. Mr. Klatu is not the one whose object turned out to be a watertower
    \+ member([klatu, watertower, _], Triples),           % Klatu did not see a watertower

    % Tells who saw what, on which day
    tell(MsBarrada, BarradaUfo, tuesday),
    tell(MsGort, GortUfo, wednesday),
    tell(MrKlatu, KlatuUfo, thursday),
    tell(MrNikto, NiktoUfo, friday).

% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.        % (1)
all_different([_ | T]) :- all_different(T).             % (2)
all_different([_]).                                     % (3)

% Checks whether one incident occured earlier in the week than another incident
% ----------------------------------------------------------------------------------------------

% Case 1
% If Scenario A is the head of the list,
% Then A will appear earlier in the list than the second argument '_'
earlier(A, _, [A | _]).

% Case 2
% If Scenario B is the head of the list,
% Then the first argument'_' will come before B
earlier(_, B, [B | _]) :- !, fail.              % If this case is true, cut (no longer need to look for solutions)

% Recurses the list if neither cases apply
earlier(A, B, [_ | C]) :- earlier(A, B, C).

% Prints out who saw what ufo on which day of the week
tell(X, Y, Z) :-
    write(X), write(' saw the '), write(Y), write(' on '), write(Z),
    write('.'), nl.

